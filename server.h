/* Include guards */
#ifndef SERVER_H_INCLUDED
#define SERVER_H_INCLUDED

/* Hash defines */
#define FOUND_RESPONSE "HTTP/1.0 200 OK\r\n"
#define NOT_FOUND_RESPONSE "HTTP/1.0 404\r\n\r\n"
#define FOUND 1
#define NOT_FOUND 0
#define BUF 100000
#define INFINITE_LOOP 1

/* Libraries used */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h> 
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/sendfile.h>

#endif